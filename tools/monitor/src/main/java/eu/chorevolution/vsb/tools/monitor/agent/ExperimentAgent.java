package eu.chorevolution.vsb.tools.monitor.agent;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.rmi.registry.LocateRegistry;
import java.util.Arrays;
import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

import eu.chorevolution.vsb.tools.monitor.mbeans.Experiment;
import eu.chorevolution.vsb.tools.monitor.util.MonitorConstant;

public class ExperimentAgent {

	ObjectName objectName = null;
	Experiment experiment = null;
	MBeanServer mBeanServer = null;

	public ExperimentAgent(String attribute, String address, int port) {

		experiment = new Experiment();

		mBeanServer = ManagementFactory.getPlatformMBeanServer();

		try {

			objectName = new ObjectName("eu.chorevolution.vsb.tools.monitor.mbeans:type=ExperimentMBean,name=" + attribute);
			mBeanServer.registerMBean(experiment, objectName);

			// Registry port for rmi communication
			LocateRegistry.createRegistry(port);
			System.setProperty("java.rmi.server.hostname",address);
			
			// Creating and starting MRI connector
			JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + address + ":" + port + "/server");
			JMXConnectorServer connector = JMXConnectorServerFactory.newJMXConnectorServer(url, null, mBeanServer);
			System.out.println("Starting RMI Connector[" + attribute + "] " + url);
			connector.start();

		} catch (

				MalformedObjectNameException | InstanceAlreadyExistsException | MBeanRegistrationException
				| NotCompliantMBeanException | IOException e) {

			e.printStackTrace();

		}
	}

	public synchronized void fireExperimentRunning(boolean experimentRunning) {

		try {

			experiment.setExperimentRunning(experimentRunning);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public synchronized void fireExperimentStartTime(long experimentStartTime) {

		try {

			experiment.setExperimentStartTime(experimentStartTime);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public synchronized void fireExperimentMsgCounter(long experimentMsgCounter) {

		try {

			experiment.setExperimentMsgCounter(experimentMsgCounter);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public synchronized void fireExperimentFinishTime(long experimentCompleteTime) {

		try {

			experiment.setExperimentCompleteTime(experimentCompleteTime);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public synchronized void fireExperimentAvgMessagesSent(double experimentAvgMessagesSent) {

		try {

			experiment.setExperimentAvgMessagesSent(experimentAvgMessagesSent);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	public synchronized void fireExperimentDuration(long experimentDuration) {

		try {

			experiment.setExperimentDuration(experimentDuration);

		} catch (IOException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	public long getNTPtimestamp() {
		   
        NTPUDPClient timeClient = new NTPUDPClient();
        InetAddress inetAddress;
        long ntpTimestamp = 0;
		try {
			
			 inetAddress = InetAddress.getByName(MonitorConstant.TIME_SERVER);
			 TimeInfo timeInfo = timeClient.getTime(inetAddress);
			 ntpTimestamp = timeInfo.getMessage().getTransmitTimeStamp().getTime();
			 timeClient.close();
		      
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ntpTimestamp;
		
	}
	
	

}
