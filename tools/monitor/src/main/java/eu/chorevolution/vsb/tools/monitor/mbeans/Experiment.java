package eu.chorevolution.vsb.tools.monitor.mbeans;

import java.io.IOException;

import javax.management.AttributeChangeNotification;
import javax.management.MBeanNotificationInfo;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;

public class Experiment extends NotificationBroadcasterSupport implements ExperimentMBean {
	
	
	private static long numeroSequence = 0l;
	private boolean experimentRunning;
	private long experimentStartTime = 0l;
	private long experimentMsgCounter = 0l;
	private long experimentCompleteTime = 0l;
	private long experimentDuration = 0l;
	private double experimentAvgMessagesSent = 0.0;
	
	
	public Experiment(){}
	

	@Override
	public MBeanNotificationInfo[] getNotificationInfo(){
		// TODO Auto-generated method stub
		final String[] types = new String[] { AttributeChangeNotification.ATTRIBUTE_CHANGE };
		final String name = AttributeChangeNotification.class.getName();
		final String description = "An attribute have been modified";
		final MBeanNotificationInfo info = new MBeanNotificationInfo(types, name, description);
		return new MBeanNotificationInfo[] { info };
	}


	@Override
	public boolean getExperimentRunning() throws IOException {
		// TODO Auto-generated method stub
		return experimentRunning;
	}


	@Override
	public void setExperimentRunning(boolean experimentRunning) throws IOException {
		// TODO Auto-generated method stub
		final Notification notification = new AttributeChangeNotification(this, numeroSequence,System.currentTimeMillis(), "experimentRunning","experimentRunning", "boolean", this.experimentRunning, experimentRunning);
		this.experimentRunning = experimentRunning;
		sendNotification(notification);
	}


	@Override
	public long getExperimentStartTime() throws IOException {
		// TODO Auto-generated method stub
		return experimentStartTime;
	}


	@Override
	public void setExperimentStartTime(long experimentStartTime) throws IOException {
		// TODO Auto-generated method stub
		final Notification notification = new AttributeChangeNotification(this, numeroSequence,System.currentTimeMillis(), "experimentStartTime","experimentStartTime", "long", this.experimentStartTime, experimentStartTime);
		this.experimentStartTime = experimentStartTime;
		sendNotification(notification);
	}


	@Override
	public long getExperimentMsgCounter() throws IOException {
		// TODO Auto-generated method stub
		return experimentMsgCounter;
	}


	@Override
	public void setExperimentMsgCounter(long experimentMsgCounter) throws IOException {
		// TODO Auto-generated method stub
		final Notification notification = new AttributeChangeNotification(this, numeroSequence,System.currentTimeMillis(), "experimentMsgCounter","experimentMsgCounter", "long", this.experimentMsgCounter, experimentMsgCounter);
		this.experimentMsgCounter = experimentMsgCounter;
		sendNotification(notification);
	}


	@Override
	public long getExperimentCompleteTime() throws IOException {
		// TODO Auto-generated method stub
		return experimentCompleteTime;
	}


	@Override
	public void setExperimentCompleteTime(long experimentCompleteTime) throws IOException {
		// TODO Auto-generated method stub
		final Notification notification = new AttributeChangeNotification(this, numeroSequence,System.currentTimeMillis(), "experimentCompleteTime","experimentCompleteTime", "long", this.experimentCompleteTime, experimentCompleteTime);
		this.experimentCompleteTime = experimentCompleteTime;
		sendNotification(notification);
	}


	@Override
	public double getExperimentAvgMessagesSent() throws IOException {
		// TODO Auto-generated method stub
		return experimentAvgMessagesSent;
	}


	@Override
	public void setExperimentAvgMessagesSent(double experimentAvgMessagesSent) throws IOException {
		// TODO Auto-generated method stub
		final Notification notification = new AttributeChangeNotification(this, numeroSequence,System.currentTimeMillis(), "experimentAvgMessagesSent","experimentAvgMessagesSent", "double", this.experimentAvgMessagesSent, experimentAvgMessagesSent);
		this.experimentAvgMessagesSent = experimentAvgMessagesSent;
		sendNotification(notification);
	}


	@Override
	public long getExperimentDuration() throws IOException {
		// TODO Auto-generated method stub
		return this.experimentDuration;
	}


	@Override
	public void setExperimentDuration(long experimentDuration) throws IOException {
		// TODO Auto-generated method stub
		final Notification notification = new AttributeChangeNotification(this, numeroSequence,System.currentTimeMillis(), "experimentDuration","experimentDuration", "long", this.experimentDuration, experimentDuration);
		this.experimentDuration = experimentDuration;
		sendNotification(notification);
	}
	
	
	

}
