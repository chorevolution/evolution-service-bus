package eu.chorevolution.vsb.tools.monitor.clocksync;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import eu.chorevolution.vsb.tools.monitor.util.MonitorConstant;

public class LocalTimeServer extends Thread{
	
	DatagramSocket serverSocket = null;
	byte[] request = new byte[1024];
	byte[] response = new byte[1024];
	ClockSync clockSync = null;
	
	public LocalTimeServer(){
		
		try {
			
			clockSync = new ClockSync();
			clockSync.startSync();
			serverSocket = new DatagramSocket(MonitorConstant.TIME_SERVER_PORT);
			System.out.println("Time server start at port : "+MonitorConstant.TIME_SERVER_PORT);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void run(){
		
		
		while(true){
			
			DatagramPacket receivePacket = new DatagramPacket(request, response.length);
			try {
				serverSocket.receive(receivePacket);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String sentence = new String( receivePacket.getData());
			InetAddress IPAddress = receivePacket.getAddress();
			int port = receivePacket.getPort();
			String timestamp = ""+System.nanoTime()+"";
			response = timestamp.getBytes();
			System.out.println(" Receive request from "+IPAddress.getHostAddress());
			DatagramPacket sendPacket = new DatagramPacket(response, response.length, IPAddress, port);
			
			try {
				serverSocket.send(sendPacket);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	public static void main(String args[]) throws Exception
	{
		
		LocalTimeServer timeServer = new LocalTimeServer();
		timeServer.start();
		
	}
	
}
