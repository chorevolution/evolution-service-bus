package eu.chorevolution.vsb.tools.monitor.mbeans;

import java.io.IOException;

import javax.management.AttributeChangeNotification;
import javax.management.MBeanNotificationInfo;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;

public class Measure extends NotificationBroadcasterSupport implements MeasureMBean {
	
	private static String name = "TimestampMBean";
	private String timestamp = "1";
	private static long numeroSequence = 0l;
	
	public Measure(){}
	

	@Override
	public MBeanNotificationInfo[] getNotificationInfo(){
		// TODO Auto-generated method stub
		final String[] types = new String[] { AttributeChangeNotification.ATTRIBUTE_CHANGE };
		final String name = AttributeChangeNotification.class.getName();
		final String description = "An attribute have been modified";
		final MBeanNotificationInfo info = new MBeanNotificationInfo(types, name, description);
		return new MBeanNotificationInfo[] { info };
	}


	@Override
	public String getTimestamp() throws IOException {
		// TODO Auto-generated method stub
		return timestamp;
	}


	@Override
	public synchronized void setTimestamp(String timestamp) throws IOException {
		// TODO Auto-generated method stub
		final Notification notification = new AttributeChangeNotification(this, numeroSequence,System.currentTimeMillis(), getName(),"timestamp", "String", this.timestamp, timestamp);
		this.timestamp = timestamp;
		sendNotification(notification);
	}

	@Override
	public String getName() throws IOException {
		// TODO Auto-generated method stub
		return this.name;
	}


	@Override
	public void setName(String name) throws IOException {
		// TODO Auto-generated method stub
		this.name = name;
	}

	

}
