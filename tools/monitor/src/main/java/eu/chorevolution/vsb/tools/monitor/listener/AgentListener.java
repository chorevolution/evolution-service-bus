package eu.chorevolution.vsb.tools.monitor.listener;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import javax.management.AttributeChangeNotification;
import javax.management.InstanceNotFoundException;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import eu.chorevolution.vsb.tools.monitor.mbeans.MeasureMBean;

public class AgentListener implements NotificationListener{

	private MBeanServerConnection mBeanServerConnection = null;
	private JMXConnector connector = null;
	private ObjectName objectName = null;
	private int port = 9000;
	private String host = "localhost";
	private String attribute = null;
	private long timestamp;
	private String message;
	private boolean isConnect = false;
	private Queue<String> queueListener;

	public AgentListener(String attribute, String host, int port){

		this.attribute = attribute;
		this.port = port;
		this.host = host;
		this.queueListener = new LinkedList<String>();

	}

	@Override
	public void handleNotification(Notification notification, Object handback){
		
		 // TODO Auto-generated method stub
		/* System.out.println("Notification received :");
		 System.out.print("\tClassName: " +
		 notification.getClass().getName());
		System.out.print("\tSource: " + notification.getSource());
		 System.out.print("\tType: " + notification.getType());
		 String message = (String) notification.getMessage();
		System.out.print("\tMessage: " + message.toString());
		 System.out.println("");*/
		// System.out.println("");

		if (notification instanceof javax.management.AttributeChangeNotification){

			final AttributeChangeNotification acn = (AttributeChangeNotification) notification;

			final Object new_value = acn.getNewValue();
			// final Object old_value = acn.getOldValue();
			String timestamp_built = (String) new_value;
			queueListener.add(timestamp_built);

		}

	}

	public void disconnect() {

		try {

			mBeanServerConnection.removeNotificationListener(objectName, this);
			connector.close();
			setConnect(false);

		} catch (InstanceNotFoundException | ListenerNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			setConnect(false);
		}
	}

	public void connect(){
		
		
		try {

			objectName = new ObjectName("eu.chorevolution.vsb.tools.monitor.mbeans:type=MeasureMBean,name=" + attribute);
			JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + host + ":" + port + "/server");
			connector = JMXConnectorFactory.connect(url, null);
			mBeanServerConnection = connector.getMBeanServerConnection();
			MeasureMBean measureMBean = (MeasureMBean) MBeanServerInvocationHandler
					.newProxyInstance(mBeanServerConnection, objectName, MeasureMBean.class, false);
			String timestamp = measureMBean.getTimestamp();
			mBeanServerConnection.addNotificationListener(objectName, this, null, null);	
			setConnect(true);

		} catch (MalformedObjectNameException | IOException | InstanceNotFoundException e){
			// TODO Auto-generated catch block
			setConnect(false);
			System.out.println(e.getMessage());
		}
	}

	public long getTimestamp(){
		
		return timestamp;
	}

	public void setTimestamp(long timestamp){
		
		this.timestamp = timestamp;
	}

	public String getMessage(){
		
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isConnect() {
		return isConnect;
	}

	public void setConnect(boolean isConnect) {
		this.isConnect = isConnect;
	}
	
	public String getMessageListened(){
		
		String data = queueListener.poll();
		if(data == null){
			
			data = "";
		}
		return data;
	}
	

}
