package eu.chorevolution.vsb.tools.monitor.main;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import eu.chorevolution.vsb.tools.monitor.listener.ExperimentListener;
import eu.chorevolution.vsb.tools.monitor.listener.MeasureListener;
import eu.chorevolution.vsb.tools.monitor.util.MonitorConstant;

/**
 * Unit test for simple App.
 */

public class MonitorSimple {
	
	public static void main(String[] args){
		// TODO Auto-generated method stub
			
		
//		if (args.length != 1) {
//
//			System.err.println("Missing arguments");
//			System.err.println("java -jar MonitorMain-jar-with-dependencies.jar  steadystateTimer");
//			System.exit(0);
//		}
//		long waitSteadystate = Long.valueOf(args[0]);
		
		long waitSteadystate = 1000; 
		
		ExperimentListener experimentListener = new ExperimentListener("StartExperiment", MonitorConstant.MDefault, "9006");
		
		HashMap<Integer, Long[]> timestampMap = new HashMap<Integer, Long[]>();
		

		
		System.out.println("Wait for steady state system...");
		new java.util.Timer().schedule(new java.util.TimerTask(){

			@Override
			public void run(){

				// TODO Auto-generated method stub
				long t0 = 0, t1 = 0;
				
				MeasureListener listener_0 = new MeasureListener("timestamp_1", MonitorConstant.MDefault, MonitorConstant.timestamp_1_port_listener,timestampMap);
				MeasureListener listener_1 = new MeasureListener("timestamp_2", MonitorConstant.MDefault, MonitorConstant.timestamp_2_port_listener,timestampMap);
				
//				MeasureListener listener_4 = new MeasureListener("timestamp_5", MonitorConstant.M4, MonitorConstant.timestamp_5_port_listener,timestampMap);
//				MeasureListener listener_5 = new MeasureListener("timestamp_6", MonitorConstant.M3, MonitorConstant.timestamp_6_port_listener,timestampMap);
//				
				int style = DateFormat.MEDIUM;
				DateFormat df = DateFormat.getDateInstance(style, Locale.US);
			    
//				 
				listener_0.connect();
				listener_1.connect();
//				listener_4.connect();
//				listener_5.connect();
				experimentListener.connect();

				try{
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				long experimentMsgSent = 0l;
				long experimentMsgReceive = 0l;
				long experimentMsgReceiveBestFitnesss = 0l;
				System.out.print("Start Monitoring.");
				while (experimentListener.experimentRunning){
					
					System.out.print(".");
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e){
						
						e.printStackTrace();
					}
					
				}
				
				try {
					
					Thread.sleep(10000);
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
//				experimentMsgReceive = listener_5.getContainer().size();
				listener_0.disconnect();
				listener_1.disconnect();
//				listener_4.disconnect();
//				listener_5.disconnect();
				
				
//				MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));
//				DB database = mongoClient.getDB("experiments");
//				DBCollection collection = database.getCollection("exp_websocket_rest_mqtt_05042017_585_100_distributed");
				
				Date start = new Date(experimentListener.experimentStartTime);
				Date complete = new Date(experimentListener.experimentCompleteTime);
				long BC1 = 0l;
				long BC2 = 0l;
				long EtoE = 0l;
				long StoBC1 = 0l;
				long BC1toBC2 = 0l;
				long BC2toR = 0l;
//                 System.out.print("\t\t t0 \t\t   \t\t t1 \t\t   \t\t t2 \t\t \t\t t3 \t\t  \t\t t4 \t\t  \t\t t5 \t\t  \t\t t1-t0 \t\t  \t\t t2-t1 \t\t  \t\t t3-t2 \t\t   \t\t t4-t3 \t\t   \t\t t5-t4 \t\t   \t\t t5-t0 \t\t\n");
				
				
				
				Set set = timestampMap.entrySet();
				Iterator iterator = set.iterator();
				while (iterator.hasNext()) {

					t0 = 0;
					t1 = 0;
					Map.Entry mentry = (Map.Entry) iterator.next();
					Long[] time_array = (Long[]) mentry.getValue();
					t0 = time_array[0];
					t1 = time_array[1];
					if(t0 != 0 && t1 != 0  ){
						
						
						EtoE += (t1 - t0);
						experimentMsgReceiveBestFitnesss++;
						
					}
					
					experimentMsgReceive++;	
				}
			      
//					DBObject doc = new BasicDBObject().append("timestamp_0", t0).append("timestamp_1", t1)
//							.append("timestamp_2", t2).append("timestamp_3", t3).append("timestamp_4", t4)
//							.append("timestamp_5", t5).append("latency_bc_1", (t2 - t1)).append("latency_bc_2", (t4 - t3))
//							.append("latency_end_to_end", (t5 - t0));
//					collection.insert(doc);

			
				long AVG_EtoE = EtoE / (experimentMsgReceiveBestFitnesss);
				
				System.out.println("\n=========================================================== ");
				System.out.println("Start time: " +start );
				System.out.println("Finish time: " +complete );
				System.out.println("Messages sent: " + experimentListener.experimentMsgCounter );
				System.out.println("Messages receive: " + experimentMsgReceive);
				System.out.println("Messages lost: " + (experimentListener.experimentMsgCounter - experimentMsgReceive) );
				System.out.println("Messages take for computing : " +experimentMsgReceiveBestFitnesss );
				System.out.println("Throughput  : " +( (double)experimentMsgReceive / 1200000) );
				System.out.println("Losses : " + ( (double) ((double)experimentMsgReceive /  (double) experimentListener.experimentMsgCounter)));
				System.out.println("Message / seconde : " + experimentListener.experimentAvgMessagesSent );
				System.out.println("End to End Avg latency: " + AVG_EtoE );
				System.out.println("=========================================================== \n");
				
			}
		}, waitSteadystate);

	}
	
	

}
