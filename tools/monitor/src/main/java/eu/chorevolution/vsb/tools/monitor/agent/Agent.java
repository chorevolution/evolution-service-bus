package eu.chorevolution.vsb.tools.monitor.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.rmi.registry.LocateRegistry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

import eu.chorevolution.vsb.tools.monitor.mbeans.Measure;
import eu.chorevolution.vsb.tools.monitor.util.MonitorConstant;

public class Agent {

	private ObjectName objectName = null;
	private Measure measure = null;
	private String attribute = null;
	private long deltaTimestamp = 0l;

	public Agent(String attribute, String address, int port){

		this.attribute = attribute;
		measure = new Measure();
		synchronized (measure) {
			
			try {

				measure.setName(attribute);

			} catch (IOException e){
				
				e.printStackTrace();
			}

			MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();

			try {
				

				objectName = new ObjectName("eu.chorevolution.vsb.tools.monitor.mbeans:type=MeasureMBean,name="+attribute);
				mBeanServer.registerMBean(measure, objectName);					

				// Registry port for rmi communication
				LocateRegistry.createRegistry(port);
				System.setProperty("java.rmi.server.hostname",address);

				// Creating and starting RMI connector
				JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + address + ":" + port + "/server");
				JMXConnectorServer connector = JMXConnectorServerFactory.newJMXConnectorServer(url, null, mBeanServer);
				System.out.println("Starting RMI Connector[" + measure.getName() + "] " + url);
				connector.start();

			} catch (

					MalformedObjectNameException | InstanceAlreadyExistsException | MBeanRegistrationException
					| NotCompliantMBeanException | IOException e) {

				e.printStackTrace();

			}
			
			// computeDeltaTime();
			
		}
		
	}
	
	public synchronized String getMessageID(String message){
		
		String  messageID = "000000000000";
		if(message.split("-")[1] != null){
			
			messageID = message.split("-")[1];
			
		}
		return messageID;
	}
	public synchronized void fire(String message){
		
		
		// long adjustedTimestamp = clockSync.getAdjustedTime();
		//long localServerTimestamp = getTimestamp();
		try{
			
			/*long timestamp = System.nanoTime();// Long.valueOf(timestamp_fired.split("-")[0]).longValue();
			String timestamp_built = (timestamp + getDeltaTime())+"-"+message+"-"+attribute;*/
			
			long timestamp = System.currentTimeMillis();// Long.valueOf(timestamp_fired.split("-")[0]).longValue();
			String timestamp_built = timestamp+"-"+message+"-"+attribute;
			measure.setTimestamp(timestamp_built);
			 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
   public synchronized String getAttribute(){
	   
	   return this.attribute;
   }
   
   public synchronized void fire2(long timestamp, String id) {
		
		
		
		try{
				
			 // String timestamp_built = (timestamp + getDeltaTime())+"-"+id;
			String timestamp_built = (timestamp + getDeltaTime())+"-"+id;
			 measure.setTimestamp(timestamp_built);
			 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	
	public long getTimestamp(){
		
		   
		BufferedReader inFromUser =
		new BufferedReader(new InputStreamReader(System.in));
		DatagramSocket clientSocket = null;
		InetAddress IPAddress = null;
		byte[] request = new byte[1024];
		byte[] response = new byte[1024];
		long timestamp = 0l;
		
		try{
			
			clientSocket = new DatagramSocket();
			IPAddress = InetAddress.getByName(MonitorConstant.TIME_SERVER);
			request = "".getBytes();
			DatagramPacket sendPacket = new DatagramPacket(request, request.length, IPAddress, MonitorConstant.TIME_SERVER_PORT);
			clientSocket.send(sendPacket);
			DatagramPacket receivePacket = new DatagramPacket(response, response.length);
			clientSocket.receive(receivePacket);
			String receive = new String(receivePacket.getData());
			clientSocket.close();
			timestamp = Long.valueOf(receive.trim()).longValue();
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return timestamp;
		
		
	}
	
	public long getDeltaTime(){
		
		return this.deltaTimestamp;
	}


	public void computeDeltaTime(){
		
		long[] deltaClientServerTime = new long[11];
		for(int i = 0; i<11; i++){
			
			long localTime_1 = System.nanoTime();
			long ntpTimestamps = getTimestamp();
			long localTime_2 = System.nanoTime();
			deltaClientServerTime[i] = 	ntpTimestamps -  localTime_2 + (long) Math.round((localTime_2 -localTime_1)/2);
			try {
				
				Thread.sleep(1000);
				
			} catch (InterruptedException e) {e.printStackTrace();}
		}
		
		System.out.print("Unsorted samples delta : ");
		for(int i = 0; i<11; i++){
			
			System.out.print(deltaClientServerTime[i]+"\t");
		}

		Arrays.sort(deltaClientServerTime);
		System.out.print("\nSorted samples delta : ");
		for(int i = 0; i<11; i++){
			                                                                                                                                                                                          
			System.out.print(deltaClientServerTime[i]+"\t");
		}
		System.out.println("");
		
		// Compute Confidence Interval
		long standardDeviation = computeSTDEV(deltaClientServerTime);
		System.out.println(" standardDeviation "+standardDeviation);
		long medianeDelta = deltaClientServerTime[5];
		List<Long> list = new ArrayList<Long>();
		for(int i = 0; i < deltaClientServerTime.length; i++){
			
			if(deltaClientServerTime[i] ==  medianeDelta){
				
				list.add(deltaClientServerTime[i]);
			}
			else if(deltaClientServerTime[i] < medianeDelta){
				
				if((medianeDelta - standardDeviation) >= deltaClientServerTime[i]){
					
					list.add(deltaClientServerTime[i]);
				}
			}
			else{
				
				if((medianeDelta + standardDeviation) <= deltaClientServerTime[i]){
					
					list.add(deltaClientServerTime[i]);
				}

			}
			list.add(deltaClientServerTime[i]);
		}
		
		long sum = 0l;
		for(int i = 0; i < list.size(); i++){
			
			sum += list.get(i); 
		}
		this.deltaTimestamp = sum / list.size();
		System.out.println("Delta deltaTimestamp "+attribute+" : "+this.deltaTimestamp);
	}
	
	public long computeSTDEV(long samples[]){
		
		// Taking the average to numbers
		long sum = 0l;
		for(int i =0; i< samples.length; i++){
			
			   sum = sum + samples[i];
		}
		long mean = sum/samples.length;
		
		
		 // Taking the deviation of mean from each numbers
		long[] deviations = new long[samples.length];
		  for(int i = 0; i < deviations.length; i++){
		   deviations[i] = samples[i] - mean ;
		  }
		
		  
		  // getting the squares of deviations
		  long[] squares = new long[samples.length];
		  for(int i =0; i< squares.length; i++) {
		   squares[i] = deviations[i] * deviations[i];
		  }
		
		  // adding all the squares
		  sum = 0;
		  for(int i =0; i< squares.length; i++) {
		   sum = sum + squares[i];
		  }
		 
		// dividing the numbers by one less than total numbers
		long result = sum / (samples.length - 1);
		
		// Taking square root of result gives the standard deviation
		long stdev = (long) Math.sqrt(result);
		return stdev;
	} 
}
