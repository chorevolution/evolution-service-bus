package eu.chorevolution.vsb.gm.protocols;

public interface Manageable {
  public void start();
  public void stop();
}
