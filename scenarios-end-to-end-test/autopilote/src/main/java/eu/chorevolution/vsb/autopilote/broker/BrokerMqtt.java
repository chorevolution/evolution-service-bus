package eu.chorevolution.vsb.autopilote.broker;

import org.apache.activemq.broker.*;
import org.fusesource.mqtt.client.BlockingConnection;



public class BrokerMqtt {
	
	
  
	private BrokerService broker;
	  private int port;
	  private String ip;
	
	  
	  public BrokerMqtt(String ip, int port){
		  
	    this.broker = new BrokerService();
	    this.ip = ip;
	    this.port = port;
	  }
	  
	  public void start() {
		
	    try {
	      broker.addConnector("mqtt://"+ ip + ":" + port);
	  //    broker.addConnector("mqtt://127.0.0.1:" + port);
	    } catch (Exception e1){
	    	
	      e1.printStackTrace();
	    }
	    broker.setDataDirectory("target2");
	    try {
	      broker.start();
	    } catch (Exception e1) {
	      e1.printStackTrace();
	    }
	    Runtime.getRuntime().addShutdownHook(new Thread(){
			@Override
			
			public void run() {
				try {
					broker.stop();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	  }
	  
	  public void stop() {
	    try {
	      broker.stop();
	    } catch (Exception e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	    }
	  }
	  
}