package eu.chorevolution.vsb.autopilote.main;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.autonomous4j.constants.Constants;

import eu.chorevolution.vsb.autopilote.broker.BrokerMqtt;
import eu.chorevolution.vsb.autopilote.handler.HandlerService;

public class AutopiloteHandler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		startBrokerService();
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		startHandlerService();

	}

	private static void startHandlerService() {

		ExecutorService executor = Executors.newFixedThreadPool(1);
		executor.execute(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					new HandlerService();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	private static void startBrokerService() {

		ExecutorService executor = Executors.newFixedThreadPool(1);
		executor.execute(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				BrokerMqtt brokerService = new BrokerMqtt(Constants.MQTT_SERVER,
						Constants.MQTT_PORT);
				brokerService.start();
			}
		});
	}

}
