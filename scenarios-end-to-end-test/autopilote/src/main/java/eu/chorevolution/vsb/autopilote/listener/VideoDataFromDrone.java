package eu.chorevolution.vsb.autopilote.listener;

import java.awt.image.BufferedImage;

import com.dronecontrol.droneapi.listeners.VideoDataListener;
import eu.chorevolution.vsb.autopilote.camera.ImagesQueue;

public class VideoDataFromDrone implements VideoDataListener {
	
	private ImagesQueue imagesQueue;
	
	public VideoDataFromDrone(ImagesQueue imagesQueue){
		
		System.out.println("Instantiate VideoDataFromDrone");
		this.imagesQueue = imagesQueue;
		System.out.println("Finish Instantiate VideoDataFromDrone");
	}

	@Override
	public void onVideoData(BufferedImage paramBufferedImage) {
		// TODO Auto-generated method stub
		
		imagesQueue.publish(paramBufferedImage);
		//imagesQueue.push(img);
	}
	
	 
	
	

}
