package org.autonomous4j.constants;

public class Constants {
	
	
	public static final int MQTT_PORT = 61616;
	public static final String MQTT_SERVER = "172.16.22.141";
	public static final String DRONE_IP = "127.0.0.1";
	public static final String TOP_LEVEL_TOPIC = "MIMOVE";
	public static final int HANDLER_PORT = 1111;
	public static final int GOOD_COMMAND = 210;
	public static final int BAD_COMMAND = 211;
	public static final String TAKEOFF_LAND = "TAKEOFF_LAND";
	public static final String BLINK = "BLINK";
	public static final String MOVETO = "MOVETO";
	public static final String GREEN = "GREEN";
}
