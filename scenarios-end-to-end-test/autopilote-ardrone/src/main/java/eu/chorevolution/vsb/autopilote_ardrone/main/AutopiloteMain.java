package eu.chorevolution.vsb.autopilote_ardrone.main;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.autonomous4j.constants.Constants;

import eu.chorevolution.vsb.autopilote_ardrone.broker.BrokerMqtt;
import eu.chorevolution.vsb.autopilote_ardrone.handler.Brain;
import eu.chorevolution.vsb.autopilote_ardrone.handler.Handler;

public class AutopiloteMain{

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		startBrokerService();
		
		try {
			
			System.out.println("Wainting for the Broker to start");
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Handler.performAutopilote(new Brain());
	}
	
	
	private static void startBrokerService() {

		ExecutorService executor = Executors.newFixedThreadPool(1);
		executor.execute(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				BrokerMqtt brokerService = new BrokerMqtt(Constants.MQTT_SERVER,
						Constants.MQTT_PORT);
				brokerService.start();
			}
		});
	}

}
