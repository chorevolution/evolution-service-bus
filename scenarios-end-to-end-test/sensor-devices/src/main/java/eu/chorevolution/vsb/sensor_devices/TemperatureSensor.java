package eu.chorevolution.vsb.sensor_devices;

import java.net.InetSocketAddress;
import java.net.SocketException;

import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.config.NetworkConfig;

public class TemperatureSensor {
	
	
	private static final int COAP_PORT = 8891;// NetworkConfig.getStandard().getInt(NetworkConfig.Keys.COAP_PORT); 
	private static TemperatureResource temperatureResource = null;
	 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
			
		new PythonCaller().start();
		
		try {
			
			System.out.println("Waiting for PythonCaller to start ");
			Thread.sleep(5000);
			
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try { 
			 
            // create server 
        	
        	NetworkConfig config = NetworkConfig.getStandard().setInt(NetworkConfig.Keys.PROTOCOL_STAGE_THREAD_COUNT, 10)
                    .setString(NetworkConfig.Keys.DEDUPLICATOR, "NO_DEDUPLICATOR");
        	
        	CoapServer coapServer = new CoapServer(config, 8891);
        	
        	coapServer.addEndpoint(addEndpoints());
            
        	temperatureResource = new TemperatureResource("temperature",true);
        	coapServer.add(temperatureResource); 
            
            coapServer.start(); 
            int count = 1;
            while(true){
            	
            	temperatureResource.resourceChange();
            	try {
					
            		Thread.sleep(2000);
					
				} catch (InterruptedException e) {e.printStackTrace();}
            	
            }
 
        } catch (SocketException e) { 
            System.err.println("Failed to initialize server: " + e.getMessage()); 
        } 
		
	}
	
	
	private static CoapEndpoint addEndpoints() throws SocketException { 
        InetSocketAddress bindToAddress = new InetSocketAddress("127.0.0.1", COAP_PORT); 
     return new CoapEndpoint(bindToAddress); 
      
    } 
	
	
	

}
