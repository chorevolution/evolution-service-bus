package eu.chorevolution.vsb.sensor_devices;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.CoAP.Type;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.server.resources.CoapExchange;

public class TemperatureResource extends CoapResource {

	
	private String resource = null;
	private volatile Type type = Type.NON;
	public TemperatureResource(String name, boolean observable) {
		super(name);
		// TODO Auto-generated constructor stub
		setObservable(observable);

	}
	
	public void resourceChange(){
		
		
		String fileData = "/opt/artifacts/temp.data";
		boolean resourceChanged = false;
		try {
			
			FileReader fileReader = new FileReader(fileData);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line = null;
			if((line = bufferedReader.readLine()) != null){
				
				if(this.resource != line){
					
					resourceChanged = true;
					this.resource = line;
				}
				
			}
			bufferedReader.close();
			fileReader.close();
		} catch ( IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		if(resourceChanged){
			
			changed();
		}
		
		
		
	}
	
	
	@Override
	public void handleGET(CoapExchange exchange) {

		Response response = new Response(ResponseCode.CONTENT);
		response.setPayload(resource);
		response.setType(type);
		response.setDuplicate(false);
		exchange.respond(response);
	}

	@Override
	public void changed(){
		
		super.changed();
	}
	
	
	
	
}
