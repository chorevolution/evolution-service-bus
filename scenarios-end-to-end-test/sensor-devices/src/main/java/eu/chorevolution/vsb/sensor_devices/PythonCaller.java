package eu.chorevolution.vsb.sensor_devices;

import java.io.IOException;

public class PythonCaller extends Thread {
	
	
	
	public void run(){
		
		
		String[] cmd = new String[2];
		cmd[0] = "python"; // check version of installed python: python -V
		cmd[1] = "/opt/artifacts/temperature_monitor.py";
		 
		// create runtime to execute external command
		Runtime rt = Runtime.getRuntime();
		try {
			
			Process pr = rt.exec(cmd);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
