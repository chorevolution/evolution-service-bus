
package eu.chorevolution.vsb.sensor_devices.general;

public class Device {

	private String type_device;
	private String protocol;
	private String port;
	private String address;
	public String getType_device() {
		return type_device;
	}
	public void setType_device(String type_device) {
		this.type_device = type_device;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public Device(String type_device, String protocol, String port, String address){
		
		this.type_device = type_device;
		this.protocol = protocol;
		this.port = port;
		this.address = address;
		System.out.println("New device has been registered : \nDevice name "+getType_device()+"\nProtocol: "+getProtocol()+"\n");
	}
	
	
}
