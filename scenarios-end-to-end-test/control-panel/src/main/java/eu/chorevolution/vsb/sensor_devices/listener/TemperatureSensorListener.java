package eu.chorevolution.vsb.sensor_devices.listener;


import eu.chorevolution.vsb.sensor_devices.general.Sensor;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.autonomous4j.constants.Constants;

public class TemperatureSensorListener  extends Sensor  implements  Runnable{

	private boolean is_alert = false;
	private String command = null;
	public TemperatureSensorListener(String type_sensor, String protocol, String port, String address) {
		super(type_sensor, protocol, port, address);
		// TODO Auto-generated constructor stub
	}

	
	public boolean isAlert(){
		
		return this.is_alert;
		
	}
	
	public String getCommand(){
		
		return this.command;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		CoapClient client = new CoapClient(getProtocol()+"://"+getAddress()+":"+getPort()+"/temperature").useNONs();
		 client = client.useExecutor();
		 CoapObserveRelation relation = client.observe( 
				   
				   new CoapHandler(){ 
					     @Override public void onLoad(CoapResponse response){ 
					    	 
					      String content = response.getResponseText(); 
					      estimate(content);
					    
					     } 
					      
					     @Override public void onError(){
					    	 
					      System.err.println("OBSERVING FAILED (press enter to exit)"); 
					      
					     } 
					    }); 
	   					BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
	   					try { br.readLine(); } catch (IOException e) { }
		
	}
	
	public void estimate(String content){
		
		
		JSONParser parser = new JSONParser();
		JSONObject jsonObject = null;

		try {

			jsonObject = (JSONObject) parser.parse(content);

		} catch (ParseException e) {

			e.printStackTrace();
		}
		String temp = String.valueOf(jsonObject.get("temp")) ;
		String alert = (String) jsonObject.get("alert");
		System.out.println("Current temperature : " + temp); 
		System.out.println("Alert level : " + alert); 
		if(alert.equals("R")){
			
			
			this.is_alert = true;
			this.command =  Constants.TAKEOFF_LAND;
		}	
		else if(alert.equals("G")){
				
				
				this.is_alert = true;
				this.command =  Constants.BLINK;
				
		}else{
			
			this.is_alert = false;
			this.command = Constants.GREEN;
		}
		
	}
	
	

}
