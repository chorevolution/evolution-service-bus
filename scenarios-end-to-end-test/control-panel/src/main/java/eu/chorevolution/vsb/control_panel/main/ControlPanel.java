package eu.chorevolution.vsb.control_panel.main;

import eu.chorevolution.vsb.sensor_devices.actuator.ParrotDroneActuator;
import eu.chorevolution.vsb.sensor_devices.listener.TemperatureSensorListener;

public class ControlPanel {
	
	private static boolean observer;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ParrotDroneActuator arDroneActuator = new ParrotDroneActuator("drone", "http", "1111", "172.16.22.141");
		TemperatureSensorListener tempSensorListener = new TemperatureSensorListener("temperature", "coap", "8891",
				"172.16.22.138");
		new Thread(tempSensorListener).start();
		while (true) {
			
			if (tempSensorListener.isAlert()){
				
				if(!arDroneActuator.isParrotDroneActivate()){
					
					arDroneActuator.setParrotDroneActivate(true);
					arDroneActuator.activate(tempSensorListener.getCommand());
					
				}else{
					
					
					System.out.println("arDroneActuator is in mission");
					
				}
				
			}
			try {
				
				Thread.sleep(1000);
				
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
