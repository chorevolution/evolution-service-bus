package eu.chorevolution.vsb.sensor_devices.general;

public class Sensor {

	private String type_sensor;
	private String protocol;
	private String port;
	private String address;
	public String getType_sensor() {
		return type_sensor;
	}
	public void setType_sensor(String type_sensor) {
		this.type_sensor = type_sensor;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public Sensor(String type_sensor, String protocol, String port, String address){
		
		this.type_sensor = type_sensor;
		this.protocol = protocol;
		this.port = port;
		this.address = address;
		System.out.println("New sensor has been registered : \nSensor name "+getType_sensor()+"\nProtocol: "+getProtocol()+"\n");
	}
	
	
}
