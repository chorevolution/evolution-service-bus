package eu.chorevolution.vsb.autopilote.video;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.util.LinkedList;
import java.util.Queue;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class JFrameDisplayer extends Thread{

	private Object mutext;
	private Queue<byte[]> imagesQueue;
	private JFrame jFrame;
	
	public JFrameDisplayer(Queue imagesQueue, Object mutext){
		
		this.mutext = mutext;
		this.imagesQueue = imagesQueue;
		
		jFrame = new JFrame(); 
		jFrame.setSize(640, 360);
		jFrame.setPreferredSize(new Dimension(640, 360));
		jFrame.setVisible(true);
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	
	public void run(){
		
    	while(true){
		
			while( imagesQueue.size() == 0){
				
				synchronized(mutext){
					
					try {
						mutext.wait();
					}catch (InterruptedException e) {e.printStackTrace();}
					
				}
				
			}
		    
			byte[] imageByte = (byte[]) imagesQueue.poll();
			BufferedImage bufferedImage = new BufferedImage(640, 360, 5);
			bufferedImage.setData(Raster.createRaster(bufferedImage.getSampleModel(), new DataBufferByte(imageByte, imageByte.length), new Point() ) );

			FrameDisplayer jpannelDisplayer = new FrameDisplayer(bufferedImage);
			jFrame.getContentPane().add(jpannelDisplayer);
			jFrame.pack();
			jFrame.remove(jpannelDisplayer);
			jFrame.pack();
    	}	
	}
	
	
	
    class FrameDisplayer  extends JPanel{

	    /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private BufferedImage bufferedImage;
	   
		public FrameDisplayer(BufferedImage bufferedImage){
	        
			this.bufferedImage = bufferedImage;
			
	    }
		public void paintComponent(Graphics g) 
		{ 
		     g.drawImage(bufferedImage, 0, 0, null); 
		     repaint(); 
		}
	 
	    
	}
}
