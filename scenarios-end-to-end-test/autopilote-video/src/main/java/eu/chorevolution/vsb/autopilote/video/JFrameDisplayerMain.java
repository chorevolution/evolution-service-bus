package eu.chorevolution.vsb.autopilote.video;

import java.util.LinkedList;
import java.util.Queue;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

public class JFrameDisplayerMain {
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Queue<byte[]> imagesQueue = new LinkedList<byte[]>();
		Object mutex = new Object();
		MqttClient client;
		try {
			
			client = new MqttClient("tcp://172.16.22.141:61616", MqttClient.generateClientId());
			client.setCallback( new MqttCallBack(imagesQueue, mutex));
			client.connect();
			client.subscribe("MIMOVE/image");
			JFrameDisplayer jFrameDisplayer = new JFrameDisplayer(imagesQueue,mutex);
			jFrameDisplayer.start();
			
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
