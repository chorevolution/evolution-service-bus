package eu.chorevolution.vsb.test.generated;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "METADATA")
public class METADATA {

	@XmlElement(name = "docs", required = true)
	private List<DOCS> docs;
	@XmlElement(name = "status", required = true)
	private String status;

	public List<DOCS> getdocs(){
		
		return docs;
	}

	public void setroutes(List<DOCS> docs) {
		
		this.docs = docs;
	}

	public String getstatus() {
		
		return status;
	}

	public void setstatus(String status){
		
		this.status = status;
	}

}
