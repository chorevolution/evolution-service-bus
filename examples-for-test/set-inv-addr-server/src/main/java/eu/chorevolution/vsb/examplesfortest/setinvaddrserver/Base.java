/**
 *   Copyright 2015 The CHOReVOLUTION project
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.chorevolution.vsb.examplesfortest.setinvaddrserver;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;


@WebService(
        name = "ConfigurableService",
        targetNamespace = "http://services.chorevolution.eu/"
)
public class Base implements BaseService {

	protected String ENDPOINTADDRESS=null;
	
	@Resource
	private WebServiceContext context;

	private static List<URL> endpoints = new ArrayList<URL>(); 
	/**
	 * Each BasicService subclass has the following lifecycle:
	 * 1. start the service
	 * 2. send it its own enpoint address
	 * 3. send the service dependency data (according to Enactment Engine choreography requirements)
	 * */
	
	public Base(){
		
	}

	/* (non-Javadoc)
	 * @see org.choreos.services.Base#setInvocationAddress(java.lang.String, java.lang.String)
	 */
	/* (non-Javadoc)
     * @see eu.chorevolution.enactment.dependencies.ConfigurableService#setInvocationAddress(java.lang.String, java.lang.String, java.util.List)
     */
	@WebMethod
	public void setInvocationAddress(String role, String name, List<String> endpoints) {
		

		System.out.println("SERVICE CALLED");
		//System.out.println("setting inv. addrr to "+ endpoints); 
		System.out.println("role "+ role); 
		System.out.println("name "+ name); 
		if (endpoints.size()>0){
			System.out.println("set the invocatin address to : " +  endpoints.get(0));
		}

	}

	/**
	 * Example call: (Client1) (getPort("Client1", "http://localhost/client1?wsdl", Client1.class));
	 * @throws MalformedURLException 
	 * */
	public static <T> T getPort(String service_name, String wsdlLocation, Class<T> client_class) throws MalformedURLException{
	    T retPort = null;
	    QName serviceQName = null;
	    URL serviceUrl = null;
	    Service s;

	    serviceQName = new QName(namespace, service_name+"Service");

	    serviceUrl = new URL(wsdlLocation);

	    s = Service.create(serviceUrl, serviceQName);
	    retPort=(T) s.getPort(client_class);

	    return retPort;
	}

}
