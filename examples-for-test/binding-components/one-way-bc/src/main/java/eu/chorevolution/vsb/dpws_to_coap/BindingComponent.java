
package eu.chorevolution.vsb.dpws_to_coap;

import java.io.File;
import eu.chorevolution.vsb.bc.manager.BcManagerRestService;
import eu.chorevolution.vsb.gm.protocols.coap.BcCoapSubcomponentPushbased;
import eu.chorevolution.vsb.gm.protocols.dpws.BcDPWSSubcomponent;
import eu.chorevolution.vsb.gm.protocols.primitives.BcGmSubcomponent;
import eu.chorevolution.vsb.gmdl.tools.serviceparser.ServiceDescriptionParser;
import eu.chorevolution.vsb.gmdl.utils.BcConfiguration;
import eu.chorevolution.vsb.gmdl.utils.GmServiceRepresentation;
import eu.chorevolution.vsb.gmdl.utils.Interface;
import eu.chorevolution.vsb.gmdl.utils.PathResolver;
import eu.chorevolution.vsb.gmdl.utils.enums.RoleType;

public class BindingComponent {

    BcGmSubcomponent[][] subcomponent;
    GmServiceRepresentation gmServiceRepresentation = null;
    public BcConfiguration bcConfiguration1 = null;
    public BcConfiguration bcConfiguration2 = null;
    
    public void run() {
        java.lang.Integer intFive;
        intFive = Integer.parseInt("5");
        java.lang.Integer intOne;
        intOne = Integer.parseInt("1");
        java.lang.Integer intNine;
        intNine = Integer.parseInt("9");
        String interfaceDescFilePath;
        interfaceDescFilePath = PathResolver.myFilePath(this.getClass(),"serviceDescription.gxdl");
        gmServiceRepresentation = ServiceDescriptionParser.getRepresentationFromGIDL(interfaceDescFilePath);
        int num_interfaces = gmServiceRepresentation.getInterfaces().size();
        System.err.println(" num_interfaces "+num_interfaces);
        subcomponent = new BcGmSubcomponent[num_interfaces][2];
        for (int i = 0; (i<gmServiceRepresentation.getInterfaces().size()); i += 1) {
            Interface inter = null;
            inter = gmServiceRepresentation.getInterfaces().get(i);
            System.out.println("genfac start iteration");
            RoleType busRole;
            System.err.println(" num_interfaces "+i+" inter.getRole() "+inter.getRole());
            if (inter.getRole() == RoleType.SERVER) {
                
            	busRole = RoleType.CLIENT;
                
            } else{
            	
                busRole = RoleType.SERVER;
            }
            
            bcConfiguration1 = new BcConfiguration();
            bcConfiguration2 = new BcConfiguration();
            bcConfiguration1 .setSubcomponentRole(inter.getRole());
            bcConfiguration2 .setSubcomponentRole(busRole);
            
            String file1 = PathResolver.myFilePath(this.getClass(),"config_block1_interface_"+String.valueOf((i + intOne)));
            String file2 = PathResolver.myFilePath(this.getClass(),"config_block1_interface_"+String.valueOf((i + intOne)));
            bcConfiguration1 .parseFromJSON(gmServiceRepresentation, file1);
            bcConfiguration2 .parseFromJSON(gmServiceRepresentation, file2);
            
            subcomponent[i][0] = new BcDPWSSubcomponent(bcConfiguration1, gmServiceRepresentation);
            subcomponent[i][1] = new BcCoapSubcomponentPushbased(bcConfiguration2, gmServiceRepresentation);
            BcGmSubcomponent block1Component = subcomponent[i][0];
            BcGmSubcomponent block2Component = subcomponent[i][1];
            block1Component.setNextComponent(block2Component);
            block2Component.setNextComponent(block1Component);
            block1Component.start();
            block2Component.start();
        }
    }

    public void pause(){
        for (int i = 0; (i<gmServiceRepresentation.getInterfaces().size()); i += 1){
        	
            System.out.println("genfac stop iteration");
            BcGmSubcomponent block1Component = subcomponent[i][0];
            BcGmSubcomponent block2Component = subcomponent[i][1];
            block1Component.stop();
            block2Component.stop();
        }
    }

}
