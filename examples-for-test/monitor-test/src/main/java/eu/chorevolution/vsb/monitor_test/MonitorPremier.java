package eu.chorevolution.vsb.monitor_test;

import java.io.IOException;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class MonitorPremier {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MBeanServerConnection mBeanServerConnection = null;
		JMXConnector connector = null;
		ObjectName name = null;
		try {
			
			name = new ObjectName("eu.chorevolution.vsb.test.monitoring:type=PremierMBean");
			JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://localhost:9000/server");
			connector = JMXConnectorFactory.connect(url, null);
			mBeanServerConnection = connector.getMBeanServerConnection();
			PremierMBean premierMBean = (PremierMBean) MBeanServerInvocationHandler.newProxyInstance(mBeanServerConnection, name, PremierMBean.class, false);
			
			while(true){
				
				int valeur = premierMBean.getValeur();
				System.out.println("valeur = " + valeur);
				Thread.sleep(1000);
				
			}
			
			
		} catch (MalformedObjectNameException | IOException | InterruptedException e) {e.printStackTrace();}
		finally {
			
			if(mBeanServerConnection != null ){
				
				try{
					
					connector.close();
					
				} catch (IOException e) {e.printStackTrace();}
			}
		}
	}

}
