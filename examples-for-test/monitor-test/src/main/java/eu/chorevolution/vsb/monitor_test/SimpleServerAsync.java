package eu.chorevolution.vsb.monitor_test;

import org.restlet.Component;
import org.restlet.Server;
import org.restlet.data.Protocol;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

public class SimpleServerAsync extends ServerResource{
	
	 public static void main(String[] args) throws Exception {
	        // Create the HTTP server and listen on port 80
	        Component cmp = new Component();
	        Server server = cmp.getServers().add(Protocol.HTTP, 8080);
	        server.getContext().getParameters().add("maxQueued", "-1");
	        server.getContext().getParameters().add("socketNoDelay", "true");

	        cmp.getDefaultHost().attachDefault(SimpleServerAsync.class);
	        cmp.start();
	    }

	    @Post
	    @Put
	    public Object echoObject(Representation rep) throws ResourceException {
	        
	    	Representation result = new StringRepresentation("{}");
	        return result;
	    }
	
}
