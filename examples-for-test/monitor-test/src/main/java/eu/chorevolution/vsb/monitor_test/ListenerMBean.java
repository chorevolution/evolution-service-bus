package eu.chorevolution.vsb.monitor_test;

import java.io.IOException;
import javax.management.AttributeChangeNotification;
import javax.management.InstanceNotFoundException;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class ListenerMBean implements NotificationListener {
	
	private  MBeanServerConnection mBeanServerConnection = null;
	private  JMXConnector connector = null;
	private  ObjectName name = null;
	private  String port = "9000";
	private String host = "localhost";
	
	
	@Override
	public void handleNotification(Notification notification, Object handback) {
		// TODO Auto-generated method stub
		System.out.println("Notification received :");
		System.out.print("\tClassName: " + notification.getClass().getName());
		System.out.print("\tSource: " + notification.getSource());
		System.out.print("\tType: " + notification.getType());
		System.out.print("\tMessage: " + notification.getMessage());
		
		if (notification instanceof AttributeChangeNotification) {
			
			final AttributeChangeNotification acn = (AttributeChangeNotification) notification;
			System.out.println("\tAttributeName: " + acn.getAttributeName());
			System.out.println("\tAttributeType: " + acn.getAttributeType());
			System.out.println("\tNewValue: " + acn.getNewValue());
			System.out.println("\tOldValue: " + acn.getOldValue());
		}
		
	}
	
	public void disconnect(){
		
		try {
			
			System.out.println("Unsubscribe to NotificationListener");
			mBeanServerConnection.removeNotificationListener(name, this);
			System.out.println("Disconnection from server JMX");
			connector.close();
			
		} catch (InstanceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ListenerNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void connect(){
		
		try {
			name = new ObjectName("eu.chorevolution.vsb.test.monitoring:type=PremierMBean");
			JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://"+host+":"+port+"/server");
			connector = JMXConnectorFactory.connect(url, null);
			mBeanServerConnection = connector.getMBeanServerConnection();
			PremierMBean premierMBean = (PremierMBean) MBeanServerInvocationHandler.newProxyInstance(mBeanServerConnection, name, PremierMBean.class, false);
			final int valeur = premierMBean.getValeur();
			System.out.println("Current mbean value = " + valeur);
			premierMBean.rafraichir();
			System.out.println("Subscribe to NotificationListener" + name.toString());
			mBeanServerConnection.addNotificationListener(name, this, null, null);
			
		} catch (MalformedObjectNameException | IOException | InstanceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
