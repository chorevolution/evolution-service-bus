package eu.chorevolution.vsb.monitor_test;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;
import org.restlet.Server;
import org.restlet.data.Protocol;

public class RestServer extends ServerResource{

	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		// Create the HTTP server and listen on port 8182  
		new Server(Protocol.HTTP, 8182, RestServer.class).start();
	}
 
	@Get
	public String present(){
		return "hello, world";
	}
}
