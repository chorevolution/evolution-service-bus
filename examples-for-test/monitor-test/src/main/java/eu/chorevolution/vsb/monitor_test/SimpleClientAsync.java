package eu.chorevolution.vsb.monitor_test;

import java.util.concurrent.CountDownLatch;

import org.restlet.Client;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Uniform;
import org.restlet.data.Protocol;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ClientResource;

public class SimpleClientAsync {
	
	
	public static void main(String[] args) throws Exception {

        Client client = new Client(new Context(), Protocol.HTTP);
        client.getContext().getParameters().add("socketNoDelay", "true");
        client.getContext().getParameters().add("maxQueued", "-1");
        client.getContext().getParameters().add("lowThreads", "4");
        client.getContext().getParameters().add("maxThreads", "6");

        ClientResource res = new ClientResource("http://localhost:8080/");
        res.setNext(client);

        int requestCount = 100;
        final CountDownLatch latch = new CountDownLatch(requestCount);

        Uniform asyncResponseHandler = new Uniform() {
            @Override
            public void handle(Request request, Response response){
                try {
                	
                	
                    response.getEntity().append(System.out);
                } catch (Exception e){
                	
                    e.printStackTrace();
                    
//                } finally {
//                    latch.countDown();
//                    //the counter goes down to 10-6 here for me 
//                    System.out.println(latch.getCount());
//                    response.abort();
                }
            }
        };

        res.setOnResponse(asyncResponseHandler);

        Representation content = null;
        for (int i = 0; i < requestCount; ++i) {
            //append the request counter so we can easily identify the requests in the log
           res.setQueryValue("count", "Mon message est long");
           content = res.post(new StringRepresentation("{}"));
           System.out.println(" i = "+i); 
//            if (content != null)
//                content.append(System.out);
           Thread.sleep(10000);
        }

        //the main thread will never be released
        latch.await();

        client.stop();
    }
	
}
