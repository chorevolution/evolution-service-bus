package eu.chorevolution.vsb.manager;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang.ArrayUtils;
import org.junit.Test;

import eu.chorevolution.vsb.bc.manager.VsbOutput;
import eu.chorevolution.vsb.gmdl.utils.Constants;
import eu.chorevolution.vsb.gmdl.utils.enums.ProtocolType;
import eu.chorevolution.vsb.manager.api.VsbManager;

//import eu.chorevolution.vsb.bindingcomponent.generated.GeneratedFactory;

public class VsbManagerTest{
	
	
	@Test
	public void testGenerateWar(){
		

//		String interfaceDescriptionPath = "/home/pntumba/inria_code/repositories/smart-mobility-tourism/bindingcomponents/without_adaptation/bcNews/model/news-service-name.gidl";
//		String interfaceDescriptionPath  = "/home/pntumba/inria_code/repositories/smart-mobility-tourism/bindingcomponents/without_adaptation/bcPws/model/pws-service-name.gidl";
//		String interfaceDescriptionPath = "/home/pntumba/inria_code/repositories/smart-mobility-tourism/bindingcomponents/without_adaptation/bcParking/model/parking-service-name.gidl";
//		String interfaceDescriptionPath = "/home/pntumba/inria_code/repositories/smart-mobility-tourism/bindingcomponents/without_adaptation/bcPoi/model/poi-service-name.gidl";
//		String interfaceDescriptionPath = "/home/pntumba/inria_code/repositories/smart-mobility-tourism/bindingcomponents/without_adaptation/bcJourneyPlanner/model/journeyplanner-service-name.gidl";
//		String interfaceDescriptionPath = "/home/pntumba/inria_code/repositories/smart-mobility-tourism/bindingcomponents/without_adaptation/bcTraffic/model/traffic-service-name.gidl";
//		String interfaceDescriptionPath = "/home/pntumba/inria_code/repositories/smart-mobility-tourism/bindingcomponents/without_adaptation/bcPublicTransportation/model/publictransportation-service-name.gidl";
		
		
//		String interfaceDescriptionPath = "/home/pntumba/inria_code/repositories/urban-traffic-coordination/bindingcomponents/bcDTS-ACCIDENTS/model/dts-accidents.gidl";
//		String interfaceDescriptionPath = "/home/pntumba/inria_code/repositories/urban-traffic-coordination/bindingcomponents/bcDTS-BRIDGE/model/dts-bridge.gidl";
//		String interfaceDescriptionPath = "/home/pntumba/inria_code/repositories/urban-traffic-coordination/bindingcomponents/bcDTS-CONGESTION/model/dts-congestion.gidl";
//		String interfaceDescriptionPath = "/home/pntumba/inria_code/repositories/urban-traffic-coordination/bindingcomponents/bcDTS-WEATHER/model/dts-weather.gidl";
		
		
//		String interfaceDescriptionPath  = "/home/pntumba/inria_code/workspace/gidl/WP4/HERE.gidl";
//		String interfaceDescriptionPath  = "/home/pntumba/inria_code/repositories/WP4/GOOGLE.gidl";
//		String interfaceDescriptionPath = "/home/pntumba/inria_code/workspace/gidl/WP4/TOMTOM.gidl";
//		String interfaceDescriptionPath = "/home/pntumba/inria_code/workspace/gidl/WP4/TrafficverketRoadcondition.gidl";
//		String interfaceDescriptionPath = "/home/pntumba/inria_code/workspace/gidl/WP4/TrafficverketWeather.gidl";
//		String interfaceDescriptionPath = "/home/pntumba/inria_code/workspace/gidl/WP4/VasttrafikGenerateOAuth2Token.gidl";
//		String interfaceDescriptionPath = "/home/pntumba/inria_code/workspace/gidl/WP4/VasttrafikrequestDepartureBoard.gidl"; 
		
//		VsbManager vsbm = new VsbManager();
//		VsbOutput  vsbOutput = vsbm.generateWar(interfaceDescriptionPath,ProtocolType.SOAP, "GOOGLE");
//		System.out.println("bc_manager_servlet_port "+vsbOutput.bc_manager_servlet_port);
//		System.out.println("service_bc_port "+vsbOutput.service_bc_port);
//		System.out.println("service_port "+vsbOutput.service_port);
//		System.out.println("setinvaddr_service_port "+vsbOutput.setinvaddr_service_port);
//		System.out.println("printer_service_port "+vsbOutput.printer_service_port);
//		System.out.println("subcomponent_port "+vsbOutput.subcomponent_port);
//		
		String interfaceDescriptionPath = "src/test/resources/gidl/gidl_1.gidl";
		VsbManager vsbm = new VsbManager();
		VsbOutput vsbOutput = vsbm.generateWar(interfaceDescriptionPath,ProtocolType.SOAP, "gidl");
	    
		assertTrue(Integer.valueOf(vsbOutput.service_bc_port) >= Constants.service_bc_port_min_range && Integer.valueOf(vsbOutput.service_bc_port) <= Constants.service_bc_port_max_range);
		assertTrue(Integer.valueOf(vsbOutput.bc_manager_servlet_port) >= Constants.bc_manager_servlet_port_min_range && Integer.valueOf(vsbOutput.bc_manager_servlet_port) <= Constants.bc_manager_servlet_port_max_range);
		assertTrue(Integer.valueOf(vsbOutput.setinvaddr_service_port) >= Constants.setinvaddr_service_port_min_range && Integer.valueOf(vsbOutput.setinvaddr_service_port) <= Constants.setinvaddr_service_port_max_range);
		assertTrue(Integer.valueOf(vsbOutput.service_port) >= Constants.service_port_min_range && Integer.valueOf(vsbOutput.service_port) <= Constants.service_port_max_range);
		assertTrue(vsbOutput.war instanceof byte[]);
		assertTrue(vsbOutput.wsdl instanceof byte[]);
		
		
		interfaceDescriptionPath = "src/test/resources/gidl/gidl_2.gidl";
		vsbm = new VsbManager();
		vsbOutput = vsbm.generateWar(interfaceDescriptionPath,ProtocolType.REST, "gidl");
		assertTrue(vsbOutput.jar instanceof byte[]);
		assertFalse(ArrayUtils.isEmpty(vsbOutput.jar));
		
		
		//assertTrue(vsbm.deleteGeneratedFiles());
	}
	
}

