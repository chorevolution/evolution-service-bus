package eu.chorevolution.vsb.playgrounds.str.websockets;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import org.json.simple.JSONObject;

import eu.chorevolution.vsb.playgrounds.str.websocketsImp.WsServer;
import eu.chorevolution.vsb.tools.monitor.agent.MeasureAgent;
import eu.chorevolution.vsb.tools.monitor.util.MonitorConstant;

import java.sql.Timestamp;

public class StartServerWebSocket {

	public WsServer server = null;
	private MeasureAgent agent = null;

	public StartServerWebSocket() {

		// create a server listening on port 8090
		server = new WsServer(new InetSocketAddress(9082));
//		agent = new MeasureAgent("timestamp_1", System.currentTimeMillis(), MonitorConstant.M3,
//				MonitorConstant.timestamp_1_port_listener);

	}

	public void start() {

		server.start();

	}

	public void send(String message) {

		server.send(message);
//		String message_id = agent.getMessageID(message);
//		agent.fire("" + System.currentTimeMillis() + "-" + message_id);

	}

}
