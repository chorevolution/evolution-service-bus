package eu.chorevolution.vsb.playground.websocket.experiment;


import eu.chorevolution.vsb.experiment.utils.Exp;
import eu.chorevolution.vsb.experiment.utils.MessageGenerator;
import eu.chorevolution.vsb.playgrounds.str.websockets.StartServerWebSocket;

public class StartSourceApplication extends Thread{

	private StartServerWebSocket server = null;
	private String msg = "";
	private Exp waitDuration;
	private long averageMsgSize = 0l;
	private MessageGenerator msgGen = null;

	public StartSourceApplication(StartServerWebSocket server, Exp waitDuration, long averageMsgSize, MessageGenerator msgGen) {

		this.server = server;
		this.waitDuration = waitDuration;
		this.averageMsgSize = averageMsgSize;
		this.msgGen = msgGen;

	}

	public void run() {
		
		System.out.println("Thread sender started");
		int count = 0;
		while (StartExperiment.experimentRunning){

			synchronized (server){
				
				String message = msgGen.getMessage();
				server.send(message);
				StartExperiment.msgCounter++;
			}
			
				
				try {
//					this.sleep(1000);
					double delay = waitDuration.next() * 1000; 
					this.sleep((long)delay);

				} catch (InterruptedException e){
					
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}

		this.interrupt();
		System.out.println("StartSourceApplication-" + this.getId() + " finish the job and dies ");
	}

	
}
