package eu.chorevolution.vsb.playground.websocket.experiment;

import eu.chorevolution.vsb.experiment.utils.Parameters;
import eu.chorevolution.vsb.experiment.utils.Exp;
import eu.chorevolution.vsb.experiment.utils.MessageGenerator;
import eu.chorevolution.vsb.playgrounds.str.websockets.StartServerWebSocket;
import eu.chorevolution.vsb.tools.monitor.agent.ExperimentAgent;
import eu.chorevolution.vsb.tools.monitor.util.MonitorConstant;


public class StartExperiment {

	public static boolean experimentRunning = true;
	public static long experimentStartTime = 0l;
	public static Long msgCounter = 0L;
	public static ExperimentAgent experimentAgent = null;
	public static Exp waitDuration = null;
	public static Long averageMsgSize = 0L;
	public static int threadNumber = 5;
	public static MessageGenerator msgGen = null;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		if (args.length < 3) {

			System.err.println("Missing arguments");
			System.err.println("java -jar StartExperiment-jar-with-dependencies.jar  duration threadNumber rate");
			System.exit(0);
		}
		
		
		Parameters.experimentDuration = Long.valueOf(args[0]);
		threadNumber = Integer.valueOf(args[1]);
		Parameters.msgSendParam = Double.valueOf(args[2]);
		
//		Parameters.experimentDuration = 2*60*1000;
//		threadNumber = 2;
//		Parameters.msgSendParam =2;
		
		waitDuration = new Exp(Parameters.msgSendParam);
		int counter = 0;
		msgGen = new MessageGenerator();
		experimentAgent = new ExperimentAgent("StartExperiment", MonitorConstant.M3, 9006);
		StartServerWebSocket server = new StartServerWebSocket();
		server.start();

		try {

			Thread.sleep(30000);

		} catch (InterruptedException e){
			
			e.printStackTrace();
		}

		experimentStartTime = System.currentTimeMillis();

		while (counter < threadNumber) {

			StartSourceApplication source = new StartSourceApplication(server, waitDuration, averageMsgSize, msgGen);
			source.start();
			counter++;
		}

		new java.util.Timer().schedule(new java.util.TimerTask() {

			@Override
			public void run(){

				// TODO Auto-generated method stub
				experimentAgent.fireExperimentFinishTime(System.currentTimeMillis());
				experimentRunning = false;
				experimentAgent.fireExperimentRunning(experimentRunning);
				experimentAgent.fireExperimentStartTime(experimentStartTime);
				experimentAgent.fireExperimentFinishTime(System.currentTimeMillis());
				experimentAgent.fireExperimentMsgCounter(msgCounter);
				experimentAgent.fireExperimentAvgMessagesSent(waitDuration.average());
				experimentAgent.fireExperimentDuration(Parameters.experimentDuration);

			}
		}, Parameters.experimentDuration);

	}

}
