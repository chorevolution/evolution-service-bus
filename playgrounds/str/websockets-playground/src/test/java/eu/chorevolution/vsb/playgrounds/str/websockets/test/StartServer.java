package eu.chorevolution.vsb.playgrounds.str.websockets.test;


import java.net.InetSocketAddress;

import org.json.simple.JSONObject;

import eu.chorevolution.vsb.playgrounds.str.websocketsImp.WsServer;

public class StartServer{
	
	public WsServer server = null;
	
	public StartServer() {
		// create a server listening on port 8090
		server = new WsServer(new InetSocketAddress(9082));
//		agent = new MeasureAgent("timestamp_1",System.currentTimeMillis(),"localhost",9000);
	}
	
	void start(){
		
		server.start();
		
		int MAX=1000000;
		int i=0;
		int LAT = 25;
		int LON = 14;		
		
		try {
			Thread.sleep(30000);
			
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// Send a message
		while(true){
			
			JSONObject jsonObject = new JSONObject();
        	jsonObject.put("lat", String.valueOf( (i * System.nanoTime())));
        	jsonObject.put("lon", String.valueOf( (i * System.nanoTime())));
        	jsonObject.put("temp", String.valueOf( (i * System.nanoTime())));
        	jsonObject.put("op_name", "bridgeNextClosure");
        	String msg = jsonObject.toJSONString()+"-1";
			server.send(msg);
			long time = System.nanoTime();
			System.err.println(" endServerWebSocket send message ["+msg+"] at time "+time+" total sent messages "+(++i));
//			 agent.fire(System.currentTimeMillis()+"");
			try{
				
				Thread.sleep(100);
				
			}catch (InterruptedException e){
				
				e.printStackTrace();
			}
		}
		
	}
	
	public static void main(String[] args){
		
		StartServer server = new StartServer();
		server.start();
		
	}
	
}
