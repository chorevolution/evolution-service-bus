package eu.chorevolution.vsb.playgrounds.clientserver.testdpws;

import java.io.IOException;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.types.QName;

import eu.chorevolution.vsb.gm.protocols.dpws.DPWSDevice;
import eu.chorevolution.vsb.gm.protocols.dpws.DPWSService;
public class DpwsServiceProvider {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		JMEDSFramework.start(null);
		DPWSDevice device = new DPWSDevice();
		DpwsService service = new DpwsService();
		device.addService(service);
		try{
			
			device.start();
			
		}catch (IOException e) {e.printStackTrace();}
		
	}

}
