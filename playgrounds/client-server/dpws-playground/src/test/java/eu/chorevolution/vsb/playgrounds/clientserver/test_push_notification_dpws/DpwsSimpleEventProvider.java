package eu.chorevolution.vsb.playgrounds.clientserver.test_push_notification_dpws;

public class DpwsSimpleEventProvider extends Thread {
	
	
	private int eventCounter = 0;
	private DpwsSimpleEvent dpwsSimpleEvent = null;
	
	public DpwsSimpleEventProvider(DpwsSimpleEvent dpwsSimpleEvent){
		
		
		this.dpwsSimpleEvent = dpwsSimpleEvent;
	}
	
	@Override
	public void run(){
		
		
		while(true){
			
			
			try {
				
				Thread.sleep(5000);
				dpwsSimpleEvent.fireHelloWorldSimpleEvent(eventCounter++);
				System.err.println("  fire Event ");
			}
			catch (Exception e) {e.printStackTrace();}
		}
	}
}
