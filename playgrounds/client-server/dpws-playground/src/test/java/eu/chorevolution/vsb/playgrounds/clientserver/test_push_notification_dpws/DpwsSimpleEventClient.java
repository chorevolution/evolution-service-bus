package eu.chorevolution.vsb.playgrounds.clientserver.test_push_notification_dpws;

import java.io.IOException;
import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.client.SearchManager;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.DPWSProtocolVersion;
import org.ws4d.java.communication.connection.ip.IPNetworkDetection;
import org.ws4d.java.communication.protocol.http.HTTPBinding;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.eventing.ClientSubscription;
import org.ws4d.java.eventing.EventSource;
import org.ws4d.java.message.eventing.SubscriptionEndMessage;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.SearchParameter;
import org.ws4d.java.types.URI;

import eu.chorevolution.vsb.gm.protocols.dpws.DPWSDevice;

public class DpwsSimpleEventClient extends DefaultClient {

	public final static String DOCU_NAMESPACE = DPWSDevice.DOCU_NAMESPACE;
	final static QName service = new QName("BasicServices", DOCU_NAMESPACE);
	private ClientSubscription notificationSub = null;

	public static void main(String[] args){
		// mandatory starting of DPWS framework

		JMEDSFramework.start(args);
		DPWSProperties.getInstance().removeSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2006);

		// create client
		DpwsSimpleEventClient client = new DpwsSimpleEventClient();

		// search services
		SearchParameter search = new SearchParameter();
		search.setServiceTypes(new QNameSet(service), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		SearchManager.searchService(search, client, null);
	}

	public void serviceFound(ServiceReference servRef, SearchParameter search, String comManId) {
		
		try{

			// get event
			EventSource eventSource = servRef.getService().getEventSource(service, "DpwsEvent", null, null);
			if (eventSource != null){

				DataStructure bindings = new ArrayList();
				HTTPBinding binding = new HTTPBinding(
						
						IPNetworkDetection.getInstance().getIPAddressOfAnyLocalInterface("127.0.0.1", false), 10236,
						"/EventSink", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
				bindings.add(binding);
				notificationSub = eventSource.subscribe(this, 0, bindings, CredentialInfo.EMPTY_CREDENTIAL_INFO);
			}

		} catch (CommunicationException e){
			e.printStackTrace();
			
		} catch (InvocationException e){
			
			e.printStackTrace();
			
		} catch (IOException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ParameterValue eventReceived(ClientSubscription clientSubscription, URI actionURI,
			ParameterValue parameterValue) {

		if (clientSubscription.equals(notificationSub)) {

			String eventText = ParameterValueManagement.getString(parameterValue, "name");
			System.err.println(" Notification event received from " + eventText);

		}
		return null;

	}

	public void subscriptionTimeoutReceived(ClientSubscription clientSubscription) {

		subscriptionEndReceived(clientSubscription, SubscriptionEndMessage.WSE_STATUS_UNKNOWN);

	}

	public void subscriptionEndReceived(ClientSubscription clientSubscription, int reason){

		System.err.println(" Subscription ended ");
	}

}
