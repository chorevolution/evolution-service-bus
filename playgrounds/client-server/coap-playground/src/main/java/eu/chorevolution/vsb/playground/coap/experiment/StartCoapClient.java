package eu.chorevolution.vsb.playground.coap.experiment;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;

import eu.chorevolution.vsb.tools.monitor.agent.MeasureAgent;
import eu.chorevolution.vsb.tools.monitor.util.MonitorConstant;

import java.util.concurrent.ExecutorService;

public class StartCoapClient {
	
	
	String destination = null;
	ExecutorService executor = null;
	MeasureAgent agent = null;
	CoapClient client = null;
			
	public StartCoapClient() {
		
		agent = new MeasureAgent("timestamp_1",MonitorConstant.MDefault,MonitorConstant.timestamp_1_port_listener);
		this.destination = "coap://"+MonitorConstant.MDefault+":2991";
		client = new CoapClient(destination+"bridgeNextClosure").useNONs();
	}

	public void sendOneWayRequest(final String message){
		
		client.setURI(destination+"/bridgeNextClosure");  
		String message_id = agent.getMessageID(message);
		agent.fire(""+System.currentTimeMillis()+"-"+message_id);
        CoapResponse response = client.post(message, MediaTypeRegistry.TEXT_PLAIN);
        	
        
		//		SendCoapMessage sendCoapMessage = new SendCoapMessage(message, scope);
//		sendCoapMessage.start();
//		executor = Executors.newFixedThreadPool(2);
//		executor.execute(new Runnable() {
//			
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				CoapClient client = new CoapClient(destination+scope).useNONs();
//				client.setURI(destination+"/"+scope);   
//				agent.fire(System.currentTimeMillis());
//		        CoapResponse response = client.post(message, MediaTypeRegistry.TEXT_PLAIN);
//		        client.shutdown();	
//			}
//		});
		
			
	}

	
	
public class SendCoapMessage extends Thread{
		
		String message = null;
		String scope = null;
		public SendCoapMessage(String message, String scope){
			
			this.message = message;
			this.scope = scope;
		}
		
		public void run(){
			
			CoapClient client = new CoapClient(destination+scope).useNONs();
			client.setURI(destination+"/"+scope);  
			String message_id = agent.getMessageID(message);
			agent.fire(""+System.currentTimeMillis()+"-"+message_id);
	        CoapResponse response = client.post(message, MediaTypeRegistry.TEXT_PLAIN);
	        client.shutdown();	
	        this.interrupt();
		}
		
	}
}
