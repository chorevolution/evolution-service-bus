package eu.chorevolution.vsb.clientserveur.coapplayground;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.CoAP.Type;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.server.resources.CoapExchange;

public class HelloWorldResource extends CoapResource {
	
	private String message = null;
	private volatile Type type = Type.NON;
	public HelloWorldResource(String name, boolean observable) {
		super(name);
		// TODO Auto-generated constructor stub
		setObservable(observable);

	}

	@Override
	public void handleGET(CoapExchange exchange) {

		Response response = new Response(ResponseCode.CONTENT);
		response.setPayload(message);
		response.setType(type);
		response.setDuplicate(false);
		exchange.respond(response);
	}

	@Override
	public void changed(){
		
		super.changed();
	}

	public void changed(String message){
		
		this.message = message;
		changed();
	}
	

}
