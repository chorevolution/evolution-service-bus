package eu.chorevolution.vsb.clientserveur.coapplayground; 

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResponse; 
  
public class CoapClientPartAsync { 
 
 static boolean getResponse = false;
 public static void main(String args[]) { 
   
 
	 
   CoapClient client = new CoapClient("coap://127.0.0.1:8891/helloWorldResource").useNONs(); 
   
   //CoapResponse response = client.get(); 
   
   client = client.useExecutor();

   
 //  while(true){
   CoapObserveRelation relation = client.observe( 
		   
			   new CoapHandler(){ 
				     @Override public void onLoad(CoapResponse response){ 
				    	 
				      String content = response.getResponseText(); 
				      System.out.println("NOTIFICATION: " + content); 
				     } 
				      
				     @Override public void onError(){
				    	 
				      System.err.println("OBSERVING FAILED (press enter to exit)"); 
				      
				     } 
				    }); 
   					BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
   					try { br.readLine(); } catch (IOException e) { } 
		  // }
   					    

 }

 
}
