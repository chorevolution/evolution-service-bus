package eu.chorevolution.vsb.clientserveur.coapplayground_pushnotification;

import java.net.InetSocketAddress;
import java.net.SocketException;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.json.simple.JSONObject;


public class CoapPushData extends CoapServer {
    
    private static final int COAP_PORT = 8892;
    private static final String IP_ADDRESS = "127.0.0.1";
    private static int LAT = 1; 
    private static int LON = 1; 
    
    public CoapPushData() throws SocketException{
    	
    	
    	add(new CoapPushDataResource());
    }
    
    private void addEndpoints() throws SocketException{ 
    	
    	InetSocketAddress bindToAddress = new InetSocketAddress(IP_ADDRESS, COAP_PORT); 
        addEndpoint(new CoapEndpoint(bindToAddress)); 
      
    } 
    
    public static void main(String[] args){
    	
    	CoapPushData serveur = null;
		try {
			
			serveur = new CoapPushData();
    		serveur.addEndpoints();
    		serveur.start();
			
		}catch(SocketException e) {e.printStackTrace();}
    	
	}
    
    
    public class CoapPushDataResource extends CoapResource {

    	
    	public CoapPushDataResource() { 
            
            // set resource identifier 
            super("bridgeNextClosure"); 
             
            // set display name 
            getAttributes().setTitle("bridgeNextClosure Resource"); 
        } 

        @Override 
        public void handleGET(CoapExchange exchange){ 
             
            // respond to the request 
        	
        	JSONObject jsonObject = new JSONObject();
        	jsonObject.put("lat", String.valueOf(LAT));
        	jsonObject.put("lon", String.valueOf(LON));
        	jsonObject.put("op_name", "bridgeNextClosure");
        	String dataPushed = jsonObject.toJSONString();
        	
    		System.out.println("Push ["+dataPushed+"]");
    		exchange.respond(dataPushed); 
    		LAT++; LON++;
           
        } 
    }
		   
}
