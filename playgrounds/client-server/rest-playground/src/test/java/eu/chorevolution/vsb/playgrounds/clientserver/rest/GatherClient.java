package eu.chorevolution.vsb.playgrounds.clientserver.rest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.restlet.Client;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Protocol;

public class GatherClient {
	
	
	public GatherClient(String message){
		
		String url = "http://localhost:1112";
		Response resp = GatherClient.post(url, message);
		System.out.println(resp.getEntityAsText());
	}
	
	private static Response post(String url, String content){
		
		Request request = new Request();
		request.setResourceRef(url);
		request.setMethod(Method.POST);
		request.setEntity(content, MediaType.APPLICATION_JSON);
		Context ctx = new Context();
		Client client = new Client(ctx, Protocol.HTTP);
		System.out.println("request: " + request);
		System.out.println("request data: " + request.getEntityAsText());
		return client.handle(request);
	}
	
	private static Response get(String url, String content){
		
		Request request = new Request();
		request.setResourceRef(url);
		request.setMethod(Method.GET);
		Client client = new Client(new Context(),Protocol.HTTP);
		return client.handle(request);
	}

}
