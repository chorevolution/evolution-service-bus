package eu.chorevolution.vsb.playgrounds.clientserver.rest;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import org.json.simple.JSONObject;
import org.restlet.Client;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Header;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Parameter;
import org.restlet.data.Protocol;
import org.restlet.representation.StringRepresentation;
import org.restlet.util.Series;

public class RestClientExample {

	public static void main(String[] args) {
		
        JSONObject obj = new JSONObject();
        obj.put("temp","14");
        String content = "{'temp':'40'}";
		String url = "http://127.0.0.1:1111";
		Response resp = RestClientExample.post(url, content);
		System.out.println(resp.getStatus());
		System.out.println(resp.getEntityAsText());


		for (Header header : resp.getHeaders()) {

			System.out.println(header.getName() + " : " + header.getValue());
		}

		System.out.print(resp.getStatus().toString());
		System.out.print(resp.getEntityAsText());

	}

	private static Response post(String url, String content) {

		Request request = new Request();
		// Parameter parameter = new Parameter();
		// parameter.create("grant_type", "client_credentials");
		// request.getResourceRef().addQueryParameter(parameter);
		// parameter.create("scope", "device_1");
		// request.getResourceRef().addQueryParameter(parameter);
		Series<Header> requestHeaders = new Series(Header.class);
		requestHeaders.add("Authorization",
				"Basic ZlR1T05kNU5ES2p6cGpZMnNDSFhrZGJ5dEFJYTpwNHlLM2xuRTB0a3ZRUDFSQldjMVRIMGVYZ2Nh");
		requestHeaders.add("Content-Type", "application/x-www-form-urlencoded");
		requestHeaders.add("Accept", "*/*");

		request.getAttributes().put("org.restlet.http.headers", requestHeaders);

		request.setResourceRef(url);
		request.setMethod(Method.POST);
		StringRepresentation entity = new StringRepresentation(
				content.toString().substring(1, content.toString().length() - 1));
		entity.setMediaType(MediaType.APPLICATION_WWW_FORM);
		entity.setCharacterSet(null);
		request.setEntity(entity);

		for (Header header : request.getHeaders()) {

			System.out.println(header.getName() + " : " + header.getValue());
		}

		Context ctx = new Context();
		Client client = new Client(ctx, Protocol.HTTP);
		System.out.println("request: " + request);
		System.out.println("request data: " + request.getEntityAsText());
		return client.handle(request);
	}

	private static Response get(String url, String content) {

		Request request = new Request();

		Series<Header> requestHeaders = new Series(Header.class);
		requestHeaders.add("Authorization","Bearer 694e2652-374c-339f-b029-dce41364310a");
		request.getAttributes().put("org.restlet.http.headers", requestHeaders);

		request.setResourceRef(url);
		request.setMethod(Method.GET);

		for (Header header : request.getHeaders()) {

			System.out.println(header.getName() + " : " + header.getValue());
		}

		Client client = new Client(new Context(), Protocol.HTTP);
		return client.handle(request);
	}

}
