package eu.chorevolution.vsb.playgrounds.mqtt;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import org.json.simple.JSONObject;
import eu.chorevolution.vsb.experiment.utils.Exp;
import eu.chorevolution.vsb.experiment.utils.MessageGenerator;

public class StartSourceApplication extends Thread {

	private PublisherMqtt publisher = null;
	private String msg = "";
	private Exp waitDuration;
	private long averageMsgSize = 0l;
	private MessageGenerator msgGen = null;

	public StartSourceApplication(PublisherMqtt publisher, Exp waitDuration, long averageMsgSize,  MessageGenerator msgGen) {

		this.publisher = publisher;
		this.waitDuration = waitDuration;
		this.averageMsgSize = averageMsgSize;
		this.msgGen = msgGen;

	}

	public void run() {
		
		System.out.println("Thread sender started");
		int count = 0;
		while (StartExperiment.experimentRunning) {

			synchronized (publisher){

				publisher.publish(msgGen.getMessage(), "bridgeNextClosure");
				StartExperiment.msgCounter++;
			}
			
				
				try {
//					this.sleep(1000);
					double delay = waitDuration.next() * 1000; 
					this.sleep((long)delay);

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}
		this.interrupt();
		System.out.println("StartSourceApplication-" + this.getId() + " finish the job and dies ");
	}


}
