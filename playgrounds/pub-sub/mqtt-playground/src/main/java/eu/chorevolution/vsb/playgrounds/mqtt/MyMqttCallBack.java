package eu.chorevolution.vsb.playgrounds.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;


public class MyMqttCallBack implements MqttCallback {

//	  private MeasureAgent agent = new MeasureAgent("timestamp_6",System.currentTimeMillis(),"localhost",9005);
//	  private int countMessage = 0;
	  public void connectionLost(Throwable throwable) {
		    
		  	System.out.println("Connection to MQTT broker lost!");
		  }

		  public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
		      
			  //agent.fire(System.currentTimeMillis()+"");
			  String message = new String(mqttMessage.getPayload());
			  System.out.println(" endpoint receive message ["+message+"] total recived messages");
			 
			  /* JSONParser parser = new JSONParser(); 
			  Object obj = parser.parse(new String(mqttMessage.getPayload()));
              JSONObject jsonPayload = (JSONObject) obj;
              System.out.println("Message received:\n\t"+ jsonPayload.toString());*/
              
		  }

		  public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
		    // not used in this example
		  }

}
