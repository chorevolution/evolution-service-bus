/**
 * StartExperiment.java
 * Created on: 1 mars 2016
 */
package eu.chorevolution.vsb.playgrounds.pubsub.jms.experiment.test;

import javax.jms.JMSException;

import org.junit.Test;

import eu.chorevolution.vsb.playgrounds.pubsub.jms.Broker;
import eu.chorevolution.vsb.playgrounds.pubsub.jms.DurableSubscriber;
import eu.chorevolution.vsb.playgrounds.pubsub.jms.Publisher;

/**
 * @author Georgios Bouloukakis (boulouk@gmail.com)
 *
 */
public class StartExperiment {
  @Test
	public void startExperiment() {

//		Broker2 broker2 = new Broker2();
//		broker2.start();
//		Broker1 broker1 = new Broker1();
//		broker1.start();
//
		DurableSubscriber subscriber = new DurableSubscriber();

		try {
			subscriber.create("subscriber-durablesubscriber", "durablesubscriber.t", "durablesubscriber1", "localhost", 61616);
		} catch (JMSException e) {
			e.printStackTrace();
		}

		Publisher pub = new Publisher();

		try {
			pub.create("publisher-durablesubscriber", "durablesubscriber.t", "localhost", 61616);
		} catch (JMSException e) {
			e.printStackTrace();
		}

		String forSent = "blaa";
		
		try {
			pub.send(forSent);
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}
}
